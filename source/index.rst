Welcome to SOAFEE Blueprint's documentation!
============================================

.. toctree::
   :caption: Contents:

   introduction.rst
   blueprints/index.rst
   contributing.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
